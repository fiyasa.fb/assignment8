#include <stdio.h>
struct student {
    int studentno;
    char Name[50];
    char subject[50];
    float marks;
} s[10];
int main() {
    int i;
    printf("Enter information of students:\n");

    // storing information
    for (i = 0; i < 5; ++i) {
        s[i].studentno = i + 1;
        printf("\nStudent number %d : \n", s[i].studentno);
        printf("Enter first name: ");
        scanf("%s", &s[i].Name);
        printf("Enter subject name: ");
        scanf("%s", &s[i].subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("\n");
    printf("Displaying Information:\n");

    // displaying information
    for (i = 0; i < 5; ++i) {
        printf("\nStudent number: %d\n", i + 1);
        printf("Student name: ");
        puts(s[i].Name);
        printf("Subject Name : ");
        puts(s[i].subject);
        printf("Marks: %.2f", s[i].marks);
        printf("\n");
    }
    return 0;
}
